#include "Unpacker.h"
#include <stdio.h>

void cUnpacker::Unpack()
{
	Unpacker.GetOptions("Themida.ini");
	if (!dwOEP) //OEP ���� X
		dwOEP = OEPFinder.FindOEP();
	if (opt.bTrap)
	{
		DWORD dwOldProtect;
		VirtualProtect((LPVOID)dwOEP, 2, PAGE_EXECUTE_READWRITE, &dwOldProtect);
		*(WORD*)dwOEP = 0xFEEB;
		VirtualProtect((LPVOID)dwOEP, 2, dwOldProtect, &dwOldProtect);
	}

}

void cUnpacker::GetOptions(char *SettingFileName)
{
	ZeroMemory(&opt, sizeof(opt));
	char szOEP[10];
	DWORD dwOEP;
	GetPrivateProfileString("Option", "OEP", "", szOEP, sizeof(szOEP), SettingFileName);
	sscanf(szOEP, "%0x8x", &opt.dwOEP);
	char szTrap[5];
	GetPrivateProfileString("Option", "Trap", "", szTrap, sizeof(szTrap), SettingFileName);
	opt.bTrap = atoi(szTrap);
}
