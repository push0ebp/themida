#pragma once

#include <Windows.h>

class cOEPFinder
{
public:
	DWORD m_afterRegs[8], m_beforeRegs[8];
	DWORD m_poZwFreeVirtualMemory;
	DWORD m_ImageBase = 0x00400000;
	PIMAGE_DOS_HEADER m_DosHeader;
	PIMAGE_NT_HEADERS m_NtHeader;
	DWORD m_dwCodeSize, m_dwCodeBase;
	DWORD m_dwOldProtect;

	DWORD OEP;

	DWORD FindOEP();
	void BreakCodes();
	LONG WINAPI ExceptionFilter(PEXCEPTION_POINTERS ExceptionInfo);

	void *DetourCreate(BYTE *src, CONST BYTE *dst, CONST INT len)
	{
		BYTE *jmp = (BYTE *)VirtualAlloc((LPVOID)NULL, len + 5, MEM_COMMIT, PAGE_EXECUTE_READWRITE);
		DWORD dwback;
		VirtualProtect(src, len, PAGE_READWRITE, &dwback);
		memcpy(jmp, src, len);
		jmp += len;
		jmp[0] = 0xE9;
		*(DWORD *)(jmp + 1) = (DWORD)(src + len - jmp) - 5;
		src[0] = 0xE9;
		*(DWORD *)(src + 1) = (DWORD)(dst - src) - 5;
		for (INT i = 5; i < len; i++) src[i] = 0x90;
		VirtualProtect(src, len, dwback, &dwback);

		return(jmp - len);
	}
};

extern cOEPFinder OEPFinder;