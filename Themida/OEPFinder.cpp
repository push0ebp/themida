#include <stdio.h>

#include "OEPFinder.h"
__declspec(naked) void HookZwFreeVirtualMemory()
{
	__asm
	{
		pushad
		mov esi, esp
		lea edi, OEPFinder.m_beforeRegs
		mov ecx, 8
		rep movsd

	}
	if (!memcmp(OEPFinder.m_afterRegs, OEPFinder.m_beforeRegs, sizeof(OEPFinder.m_afterRegs)))
		OEPFinder.BreakCodes();
	__asm
	{
		mov esi, esp
		lea edi, OEPFinder.m_afterRegs
		mov ecx, 8
		rep movsd
		popad
		jmp[OEPFinder.m_poZwFreeVirtualMemory]
	}
}


LONG WINAPI cOEPFinder::ExceptionFilter(PEXCEPTION_POINTERS ExceptionInfo) {
	DWORD ExceptionAddr = (DWORD)ExceptionInfo->ExceptionRecord->ExceptionAddress;
	DWORD ExceptionCode = ExceptionInfo->ExceptionRecord->ExceptionCode;
	PCONTEXT c = ExceptionInfo->ContextRecord;
	if ((ExceptionCode == 0xC0000096 && *(BYTE*)ExceptionAddr == 0xFB) || //sti PRIV_INSTRUCTION for Themida
		!(m_dwCodeBase <= ExceptionAddr && ExceptionAddr <= m_dwCodeBase + m_dwCodeSize ))
		return EXCEPTION_CONTINUE_SEARCH;
	OEP = c->Eip;
	//VirtualProtect((LPVOID)m_dwCodeBase, m_dwCodeSize, m_dwOldProtect, &m_dwOldProtect);
	return EXCEPTION_CONTINUE_EXECUTION;
}

LONG WINAPI CallExceptionFilter(PEXCEPTION_POINTERS ExceptionInfo)
{
	return OEPFinder.ExceptionFilter(ExceptionInfo);
}


void cOEPFinder::BreakCodes()
{
	m_dwCodeBase = m_ImageBase + m_NtHeader->OptionalHeader.BaseOfCode;
	PIMAGE_SECTION_HEADER CodeSection = IMAGE_FIRST_SECTION(m_NtHeader);
	m_dwCodeSize = CodeSection->Misc.VirtualSize;
	SetProcessDEPPolicy(TRUE);
	VirtualProtect((LPVOID)m_dwCodeBase, m_dwCodeSize, PAGE_NOACCESS, &m_dwOldProtect);
	AddVectoredExceptionHandler(1, CallExceptionFilter);
}

DWORD cOEPFinder::FindOEP()
{
	m_DosHeader = (PIMAGE_DOS_HEADER)m_ImageBase;
	m_NtHeader = (PIMAGE_NT_HEADERS)((DWORD)m_DosHeader + m_DosHeader->e_lfanew);
	m_poZwFreeVirtualMemory = (DWORD)DetourCreate((PBYTE)GetProcAddress(GetModuleHandle("ntdll.dll"), "ZwFreeVirtualMemory"), (PBYTE)&HookZwFreeVirtualMemory, 5);

	WORD *EntryPoint = (WORD*)(m_ImageBase + m_NtHeader->OptionalHeader.AddressOfEntryPoint);
	
	*EntryPoint = 0x5056; //Resume
	int ntime = GetTickCount();
	while (!(OEP && GetTickCount() - ntime < 60 * 1 * 1000)) // Timeout 1 minute
		Sleep(1000);
	return OEP;
}
