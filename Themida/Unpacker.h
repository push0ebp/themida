#pragma once
#include <Windows.h>

#include "OEPFinder.h"

class cUnpacker
{
public:
	cOEPFinder OEPFinder;

	struct sOption
	{
		DWORD dwOEP;
		int bTrap;
	} opt;

	DWORD dwOEP;

	void Unpack();
	void GetOptions(char *SettingFileName);
};

extern cUnpacker Unpacker;