#pragma once
#include <Windows.h>

class cPE
{
public:
	DWORD m_ImageBase;
	PIMAGE_DOS_HEADER m_DosHeader;
	PIMAGE_NT_HEADERS m_NtHeader;

	cPE(DWORD ImageBase)
	{
		m_ImageBase = ImageBase;
		m_DosHeader = (PIMAGE_DOS_HEADER)m_ImageBase;
		m_NtHeader = (PIMAGE_NT_HEADERS)((DWORD)m_DosHeader + m_DosHeader->e_lfanew);
	}

};