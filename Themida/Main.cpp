#include <Windows.h>
#include <stdio.h>
#include "OEPFinder.h"
#include "Unpacker.h"

cOEPFinder OEPFinder;
cUnpacker Unpacker;

void Main()
{
	Unpacker.OEPFinder = OEPFinder;
	Unpacker.Unpack();
}


BOOL __stdcall DllMain(HMODULE hDll, DWORD dwReason, LPVOID lpReserved)
{
	DisableThreadLibraryCalls(hDll);
	if (dwReason == DLL_PROCESS_ATTACH)
	{
		if (AllocConsole()) {
			freopen("CONIN$", "rb", stdin);
			freopen("CONOUT$", "wb", stdout);
			freopen("CONOUT$", "wb", stderr);
			SetConsoleTitle("OEP Finder");
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE);
		}
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)Main, NULL, NULL, NULL);
	}
	return TRUE;
}
