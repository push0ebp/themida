#include <Windows.h>
#include <stdio.h>


bool InjectDLL(char *dllName, DWORD pId)
{
	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, false, pId);
	if (hProcess)
	{
		LPVOID libAddr = (LPVOID)GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");
		LPVOID pathAddr = VirtualAllocEx(hProcess, NULL, strlen(dllName), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
		WriteProcessMemory(hProcess, pathAddr, dllName, strlen(dllName), NULL);
		HANDLE hThread = CreateRemoteThread(hProcess, NULL, NULL, (LPTHREAD_START_ROUTINE)libAddr, pathAddr, 0, NULL);
		WaitForSingleObject(hThread, INFINITE);
		VirtualFreeEx(hProcess, pathAddr, strlen(dllName), MEM_RELEASE);
		CloseHandle(hThread);
		CloseHandle(hProcess);
		return true;
	}
	return false;
}



void main(int argc, char **argv)
{
	STARTUPINFO si = {0};
	PROCESS_INFORMATION pi = {0};
	char *FileName = argv[1];
	char *DLLName = "Themida.dll";
	if (!CreateProcessA(FileName, NULL, NULL, NULL, FALSE, CREATE_SUSPENDED, NULL, NULL, &si, &pi))
	{
		puts("[-] Error : CreateProcess failed");
	}
	puts("[+] CreateProcess");

	HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pi.dwProcessId);
	if (!hProcess)
	{
		puts("[-] Error : OpenProcess failed");
		printf("  hProcess = %d\n", hProcess);
	}
	BYTE Header[0x400] = { 0 };
	DWORD ImageBase = 0x00400000;
	if (!ReadProcessMemory(hProcess, (LPCVOID)ImageBase, Header, sizeof(Header), NULL))
		puts("[-] Error : ReadProcessMemory failed");
	if (*(WORD*)Header != 0x5A4D)
		puts("[-] Error : invalid PE ");

	PIMAGE_DOS_HEADER DosHeader = (PIMAGE_DOS_HEADER)Header;
	PIMAGE_NT_HEADERS NtHeader = (PIMAGE_NT_HEADERS)((DWORD)DosHeader + DosHeader->e_lfanew);
	DWORD *EntryPoint = (DWORD*)(ImageBase + NtHeader->OptionalHeader.AddressOfEntryPoint);
	printf("[+] EntryPoint = %08X\n", EntryPoint);
	WORD PatchBytes = 0xFEEB;
	WriteProcessMemory(hProcess, EntryPoint, &PatchBytes, sizeof(PatchBytes), NULL);
	
	ResumeThread(pi.hThread);//for injecting dll
	puts("[+] Resumed");

	InjectDLL(DLLName, pi.dwProcessId);
	puts("[+] Injected DLL");
	WaitForSingleObject(pi.hProcess, INFINITE);
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);

}